export function fight(firstFighter, secondFighter) {
  while (true) {
    secondFighter.health -= getDamage(firstFighter, secondFighter);
    if (secondFighter.health < 0) {
      return firstFighter;
    }

    firstFighter.health -= getDamage(secondFighter, firstFighter);
    if (firstFighter.health < 0) {
      return secondFighter;
    }
  }
}

export function getDamage(attacker, enemy) {
  const damage = getHitPower(attacker) - getBlockPower(enemy);

  return damage > 0 ? damage : 0;
}

export function getHitPower(fighter) {
  const criticalHitChance = Math.random(1) + 1;
  const hitPower = fighter.attack * criticalHitChance;

  return hitPower;
}

export function getBlockPower(fighter) {
  const dodgeChance = Math.random(1) + 1;
  const blockPower = fighter.defense * dodgeChance;

  return blockPower;
}
