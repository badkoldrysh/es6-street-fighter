import { showModal } from "./modal";
import { createElement } from "../helpers/domHelper";

export  function showWinnerModal(fighter) {
  const fighterName = fighter.name;
  const title = "We have the winner!";
  const bodyElement = createBodyElement(fighterName);

  showModal({ title, bodyElement });
}

function createBodyElement(fighterName) {
  const bodyElement = createElement( { tagName: 'div', className: 'winner-modal' });
  const winnerName = createElement( { tagName: 'span', className: 'winner-name' });

  winnerName.innerText = fighterName;
  bodyElement.append(winnerName);

  return bodyElement;
}