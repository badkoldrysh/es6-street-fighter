import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';

export  function showFighterDetailsModal(fighter) {
  const title = 'Fighter info';
  const bodyElement = createFighterDetails(fighter);
  showModal({ title, bodyElement });
}

function createFighterDetails(fighter) {
  const { name, attack, defense, health, source } = fighter;

  const fighterDetails = createElement({ tagName: 'div', className: 'modal-body' });
  const infoBlockElement = createElement({ tagName: 'div', className: 'fighter-info' });
  const nameElement = createElement({ tagName: 'span', className: 'fighter-name' });
  const attackElement = createElement({ tagName: 'span', className: 'fighter-attack' });
  const defenseElement = createElement({ tagName: 'span', className: 'fighter-defense' });
  const healthElement = createElement({ tagName: 'span', className: 'fighter-health' });
  const imageElement = createElement({ tagName: 'img', className: 'fighter-image', attributes: { src: source } });

  nameElement.innerText = name;
  attackElement.innerText = 'Attack: ' + attack;
  defenseElement.innerText = 'Defense: ' + defense;
  healthElement.innerText = 'Health: ' + health;
  infoBlockElement.append(nameElement);
  infoBlockElement.append(attackElement);
  infoBlockElement.append(defenseElement);
  infoBlockElement.append(healthElement);

  fighterDetails.append(imageElement);
  fighterDetails.append(infoBlockElement);

  return fighterDetails;
}
